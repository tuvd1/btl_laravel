@extends('layouts.form')
@section('page_content')
    <div class="card-body">
        <form action="{{ route('song.update', $songs->id) }}" enctype="multipart/form-data" class="row" method="post">
            @csrf
            @method('PUT')
            <div class="col-12 mb-4">
                <input type="text" class="form-control" placeholder="Song name" name="name" value="{{ $songs->name }}" />
            </div>
            <div class="col-12 mb-4">
                <label class="form-label">Picture</label>
                <input type="file"  name="file_picture" class="form-control" value="{{ $songs->picture }}">
            </div>
            <div class="col-sm-6 mb-4">
                <label class="form-label">Song file</label>
                <input type="file"  name="file_song" class="form-control" value="{{ $songs->path }}">
            </div>
            <select  name="category_id[]" class="col-12 mt-4 select2-role" style="background-color: #151719ed;" multiple data-live-search="true">
                <option selected="selected" disabled="disabled" hidden=""></option>

                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach


            </select>
            <select  name="singer_id" class="col-12 mb-4 select2-role"  aria-label="Select Singer">
                <option selected="selected" disabled="disabled" hidden="">Select Song</option>
                @foreach ($singers as $singer)
                <option value="{{ $singer->id }}">{{ $singer->name }}</option>
                @endforeach
            </select>
            <div class="card-footer text-center"><button  type="submit" class="btn btn-primary" style="min-width: 140px;">Edit</button> </div>
        </form>
    </div>
@endsection
