@extends('layouts.form')
@section('page_content')
    <div class="section">
        <div class="row">
            <div class="col-xl-7 col-md-10 mx-auto">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="mat-tabs">
                            <ul class="nav nav-tabs" id="add_music" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link"><a href="{{ route('song.create') }}">Add Song</a> </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link"><a href="{{ route('album.create') }}">Add Album</a> </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link"><a href="{{ route('singer.create') }}">Add Singer</a> </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link"><a href="{{ route('category.create') }}">Add Category</a> </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @yield('add')

                </div>
            </div>
        </div>
    </div>
@endsection
