@extends('layouts.form')
@section('page_content')
    <div class="card-body">
        <form action="{{ route('singer.update', $singers->id) }}" enctype="multipart/form-data" class="row" method="post">
            @csrf
            @method('PUT')
            <div class="col-12 mb-4">
                <input type="file" name="file" value="{{ $singers->path }}">
            </div>
            <div class="col-12 mb-4">
                <input type="text" class="form-control" placeholder="Name Singer" name="name" value="{{ $singers->name }}"/>
            </div>
            <div class="card-footer text-center"><button  type="submit" class="btn btn-primary" style="min-width: 140px;">Edit</button> </div>
        </form>
    </div>

@endsection
