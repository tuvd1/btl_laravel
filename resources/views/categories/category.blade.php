@extends('layouts.form')
@section('page_content')
<div class="under-hero container">
    <div class="section">
        <div class="section__head">
            <h3 class="mb-0">Music <span class="text-primary">Category</span></h3>
        </div>

        <div class="row g-4">
            @foreach ($categories as $category)
            <div class="col-xl-3 col-sm-6">
                <div class="cover cover--round cover__image">
                    <a href="" class="cover__image">
                        <img src="{{ $category->picture }}" alt="Remix" />
                        <div class="cover__image__content"><span class="cover__title mb-1 fs-6 text-truncate">{{ $category->name }}</span></div>
                    </a>
                    <a href="{{ route('category.edit', $category->id) }}">
                        <button type='submit' class='btn bg-yellow    btn-flat'  name=''>Sửa</button>
                    </a>
                    <form action="{{ route('category.destroy', $category->id) }}" method="POST" onclick="return confirm('Are you sure?')">
                        @csrf
                        @method('DELETE')
                        <button type='submit' class='btn bg-red btn-flat'>Xóa</button></a>
                    </form>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
@endsection
