@extends('layouts.add')
@section('add')
    <div class="card-body">
        <form action="{{ route('category.store') }}" enctype="multipart/form-data" class="row" method="post">
            @csrf
            <div class="col-12 mb-4">
                {{-- <div class="dropzone text-center">
                    <div class="dz-message">
                        <i class="ri-upload-cloud-2-line fs-2 text-dark"></i> <span class="d-block fs-6 mt-2">Drag & Drop or click to Upload</span>
                        <span class="d-block form-text mb-4">540x320 (Max: 300KB)</span> <button type="button" class="btn btn-light-primary">Upload cover image</button>
                    </div>
                </div> --}}
                <input type="file" name="file">
            </div>
            <div class="col-12 mb-4">
                <input type="text" class="form-control" placeholder="Name Category" name="name" />
            </div>
            <div class="card-footer text-center"><button  type="submit" class="btn btn-primary" style="min-width: 140px;">ADD</button> </div>
        </form>
    </div>

@endsection
