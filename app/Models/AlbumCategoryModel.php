<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumCategoryModel extends Model
{
    use HasFactory;
    protected $table = "album_category";
    protected $fillable = ['category_id', 'album_id'];
}

