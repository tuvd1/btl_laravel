<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    use HasFactory;
    protected $table = 'categories';
    protected $fillable = ['name', 'picture'];
    public function albums()
    {
        return $this->belongsToMany(AlbumModel::class, 'album_category', 'category_id', 'album_id');
    }
    public function songs()
    {
        return $this->belongsToMany(SongModel::class, 'category_song', 'category_id', 'song_id');
    }
}
