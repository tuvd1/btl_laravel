<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SingerModel extends Model
{
    use HasFactory;
    protected $table = 'singers';
    protected $fillable = ['name', 'picture'];
    public function albums()
    {
        return $this->belongsToMany(AlbumModel::class, 'album_singer', 'singer_id', 'album_id');
    }
    public function songs()
    {
        return $this->hasMany(SongModel::class, 'singer_id', 'id');
    }
}
