<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumSongModel extends Model
{
    use HasFactory;
    protected $table = "album_song";
    protected $fillable = ['song_id', 'album_id'];
}
