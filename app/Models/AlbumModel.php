<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumModel extends Model
{
    use HasFactory;
    protected $table = 'albums';
    protected $fillable = ['name', 'picture', 'created_by'];
    public function categories()
    {
        return $this->belongsToMany(CategoryModel::class, 'album_category', 'album_id', 'category_id');
    }

    public function songs()
    {
        return $this->belongsToMany(SongModel::class, 'album_song', 'album_id', 'song_id');
    }
    public function singers()
    {
        return $this->belongsToMany(SingerModel::class, 'album_singer', 'album_id', 'singer_id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
